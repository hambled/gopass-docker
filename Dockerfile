FROM golang:alpine

WORKDIR /gopass

# Downloads.
RUN apk add bash gnupg git nano exa fzf wget xclip tzdata                                        && \
    go get github.com/gopasspw/gopass                                                            && \
    wget -O /gopass/.bashrc       https://gitlab.com/hambled/dotfiles/-/raw/master/.bashrc       && \
    wget -O /gopass/.bash_aliases https://gitlab.com/hambled/dotfiles/-/raw/master/.bash_aliases

# Config 1.
RUN cat /usr/share/zoneinfo/UTC > /etc/localtime
ENV SECRETO=/gopass/gopass
ENV GNUPGHOME=$SECRETO/.gnupg
ENV GOPASS_GPG_OPTS=--homedir=$GNUPGHOME
ENV GOPASS_HOMEDIR=$SECRETO
ENV GOPASS_FORCE_UPDATE=1

# Helper script.
RUN echo '#!/usr/bin/env bash'                                   > init-gpg-and-gopass.sh && \
    echo "echo -e '\e[36mSi no sabes que elegir, prueba:\e[0m'" >> init-gpg-and-gopass.sh && \
    echo "echo -e '    \e[33m(9) ECC and ECC\e[0m'"             >> init-gpg-and-gopass.sh && \
    echo "echo -e '    \e[33m(1) Curve 25519\e[0m'"             >> init-gpg-and-gopass.sh && \
    echo echo                                                   >> init-gpg-and-gopass.sh && \
    echo                                                        >> init-gpg-and-gopass.sh && \
    echo export GNUPGHOME=$GNUPGHOME                            >> init-gpg-and-gopass.sh && \
    echo export GOPASS_GPG_OPTS=$GOPASS_GPG_OPTS                >> init-gpg-and-gopass.sh && \
    echo export GOPASS_HOMEDIR=$GOPASS_HOMEDIR                  >> init-gpg-and-gopass.sh && \
    echo mkdir -p \"$SECRETO\"                                  >> init-gpg-and-gopass.sh && \
    echo mkdir -p \"$GOPASS_HOMEDIR\"                           >> init-gpg-and-gopass.sh && \
    echo chmod -R 700 \"$SECRETO\"                              >> init-gpg-and-gopass.sh && \
    echo gpg --expert --full-generate-key \&\&                  >> init-gpg-and-gopass.sh && \
    echo gopass init                                            >> init-gpg-and-gopass.sh && \
    chmod 700 init-gpg-and-gopass.sh

# Config 2.
RUN adduser -Dh/gopass -s/bin/bash -u1000 gopass               && \
    echo 'export PS1_BANNER=GoPass'         >> /gopass/.bashrc && \
    echo 'source <(gopass completion bash)' >> /gopass/.bashrc && \
    chown -R gopass:gopass /gopass


CMD ["gopass"]
